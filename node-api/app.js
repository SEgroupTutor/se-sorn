const express = require('express')
const app =express()
const morgan = require('morgan')
const mysql = require('mysql')
const path = require('path')
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use(morgan('combined'))
app.use(express.static('../build'))

app.get('/profile/:username/:password',(req,res)=>{
  console.log("Fetching user with id: " + req.params.username )
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  const queryString = "SELECT * FROM users WHERE username LIKE '" + req.params.username + "%' and passwd = '" + req.params.password+"'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {username: row.username ,passwd: row.passwd, firstname: row.firstname,lastname: row.lastname ,contact: row.contact}
    })
    console.log("-------------------------------------" + users)
    res.json(users)
  })
})

app.post('/login' , (req,res)=>{
  var username = req.body.username
  var password = req.body.password
  console.log(req.body)
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  if(username && password){
      const queryString = "select * from users where username = '"+ req.body.username + "' and passwd = '" + req.body.password +"'"
      connection.query(queryString,(err,results,fields)=>{
        //console.log("################"+results[0].firstname)
      if(results.length>0 && results[0].Userstatus == req.body.status){
        res.sendStatus(399,)
        console.log("Login success");

    }else{
      console.log("invalid username or password")
        res.sendStatus(500)
    }
    const users = results.map((row) => {
      return {username: row.username ,passwd: row.passwd, firstname: row.firstname,lastname: row.lastname ,contact: row.contact}
    })
    console.log(users)
    res.end()
    
  });
}else{
  res.sendStatus(501)
  console.log("Please enter username and password")

  
  res.end()
}
})

app.get('/courseDel/:courseID',(req,res)=>{
  console.log("Del Course")
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  //console.log(req)
  console.log("Delete  course Where courseID ="+req.params.courseID);

  const queryString = "Delete from course Where courseID = " +req.params.courseID
  connection.query(queryString,(err,results,fields)=>{
    if(err){
      console.log("Del No")
      res.sendStatus(500)
      return
    }
    console.log("Del Yes");
    res.end()
 })
 res.end()
})
app.post('/register',(req,res)=>{
  var username = req.body.username
  var password = req.body.password
  var firstname = req.body.firstname
  var lastname = req.body.lastname
  var contact = req.body.contact
  console.log("Reg")
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
    if(username && password && firstname && lastname && contact){
      const queryString = "select * from users where username = '"+ req.body.username + "' and passwd = '" + req.body.password +"'"
      connection.query(queryString,(err,result,fields)=>{
        if(result.length>0){
          console.log("already use")
          res.sendStatus(203)
        }else{
          const queryString = "INSERT INTO users (username,passwd,firstname,lastname,contact,Userstatus) value('"+req.body.username+"','"+req.body.password+"','"+req.body.firstname+"','"+req.body.lastname+"','"+req.body.contact+"','"+req.body.status+"')"
          connection.query(queryString,(err,results,fields)=>{
       console.log(queryString)
         console.log("insert Yes")
         res.sendStatus(201)
       });
        }
      });
  }else{
      console.log("invalid input")
      res.sendStatus(501)
      res.end()
    }
})


app.post('/courses',(req,res)=>{
  console.log("PostCourse")
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  console.log(req.body)
//  console.log("INSERT INTO course (sid,subject,day) VALUES('"+ req.body.subject + "','" + req.body.day + "')");

  var detail = req.body.detail+" || จำนวนนักเรียน : " + req.body.student + " || ราคาต่อชั่วโมง : "+req.body.price + " || สถานที่สอน : " + req.body.place + " || ระยะเวลาการสอน : "+req.body.period

  const queryString = "INSERT INTO course (state,subject,day,time,detail,username,passwd) VALUES('no','"+ req.body.subject + "','" + req.body.day + "','" + req.body.time+ "','" + detail+"','"+req.body.username+"','"+req.body.password+"')"
  //console.log('hello');
  connection.query(queryString,(err,results,fields)=>{
     if(err){
       console.log("insert No")
       res.sendStatus(500)
       return
     }
     console.log("insert Yes");
     res.end(JSON.stringify(results))
  })
  res.end()
})


app.get('/course/:subject', (req, res)=>{
  console.log("Fetching user with id: " + req.params.subject )
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  
  //const course = req.params.id
  const queryString = "SELECT * FROM course WHERE subject LIKE '" + req.params.subject + "%'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time,state: row.state}
    })
    res.json(users)
  })
})
app.get('/course/:day', (req, res)=>{
  console.log("Fetching user with id: " + req.params.subject +"and"+req.params.day)
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  console.log("SELECT * FROM day WHERE day LIKE '" + req.params.day + "%'" )
  
  //const course = req.params.id
  const queryString = "SELECT * FROM course WHERE day == '" + req.params.day + "%'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time,state: row.state}
    })
    res.json(users)
  })
})

app.get('/course/:subject/filter/:day', (req, res)=>{
  console.log("Fetching user with id: " + req.params.subject +"and"+req.params.day)
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  
  //const course = req.params.id
  const queryString = "SELECT * FROM course WHERE subject LIKE'" + req.params.subject+"%' and day ="+ "'" +req.params.day+"'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID  ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time,state: row.state}
    })
    res.json(users)
  })
})
app.get('/course/:subject/filter/:day/:time', (req, res)=>{
  console.log("Fetching user with id: " + req.params.subject +"and"+req.params.day+"and"+req.params.time)
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  
  //const course = req.params.id
  const queryString = "SELECT * FROM course WHERE subject LIKE'" + req.params.subject+"%' and day ="+ "'" +req.params.day+"' and time ='"+req.params.time+"'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID  ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time,state: row.state}
    })
    res.json(users)
  })
})

app.get('/course', (req, res)=>{
  console.log("Fetching user with id: " + req.params.id)
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  
  const userId = req.params.id
  const queryString = "SELECT * FROM course"
  connection.query(queryString,[userId],(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID  ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time,state: row.state}
    })
    res.json(users)
  })
})


app.get('/course12/:username/:password', (req, res)=>{
  console.log("Fetching user with id: " + req.params.username )
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  const queryString = "SELECT * FROM course WHERE username = '" + req.params.username + "'" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time,state: row.state}
    })
    res.json(users)
  })
})
app.get('/teachby/:username/:password', (req, res)=>{
  console.log("Fetching user with id: " + req.params.username )
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  const queryString = "SELECT * FROM course WHERE courseID in (select courseID from teach where username ='" + req.params.username + "' and passwd ='"+req.params.password+"')" 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")

    const users = rows.map((row) => {
      return {courseID: row.courseID ,subject: row.subject, detail: row.detail,day: row.day ,time: row.time,state:row.state}
    })
    res.json(users)
  })
})

app.post('/teach',(req,res)=>{
  console.log("teachCourse")
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  console.log(req.body)
  //onsole.log("INSERT INTO teach (courseID,username,passwd) VALUES("+ req.body.courseID + ",'" + req.body.username + "','"+req.body.password+"')");

  const queryString = "INSERT INTO teach (courseID,username,passwd,state) VALUES("+ req.body.courseID + ",'" + req.body.username + "','"+req.body.password+"',false)"
  //console.log('hello');
  console.log(queryString)
  connection.query(queryString,(err,results,fields)=>{
     if(err){
       console.log("insert No")
       res.sendStatus(500)
       return
     }
     const queryString1 = "update course set state = 'yes' where courseID ="+req.body.courseID
     connection.query(queryString1,(err2,results2,fields2)=>{
      if(err2){
        console.log("insert No")
        res.sendStatus(500)
        return}
        console.log("insert Yes");
        res.end(JSON.stringify(results2))

     })
     res.end(JSON.stringify(results))
  })
  res.end()
})

app.get("/confirm/:condition/:courseID",(req,res)=>{
  const connection = mysql.createConnection({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'password',
    database: 'sorn_mysql'
  })
  console.log("update course set state ='"+req.params.condition+"' where courseID =" +req.params.courseID )
  const queryString = "update course set state ='"+req.params.condition+"' where courseID =" +req.params.courseID 
  connection.query(queryString,(err,rows,fields)=>{
    if(err){
      console.log("Failed to query for users" + err)
      res.sendStatus(500)
      return
      //throw err
    }
    console.log("I think we fetched users successfully")
    })
  if(req.params.condition == "no"){
    console.log("update course set state ='"+req.params.condition+"' where courseID =" +req.params.courseID )
  const queryString2 = "delete from teach where courseID =" +req.params.courseID 
  connection.query(queryString2,(err2,rows,fields)=>{
    if(err2){
      console.log("Failed to query for users" + err2)
      res.sendStatus(500)
      return
      //throw err
    }
    
    console.log("I think we fetched users successfully")
    
    
    })

  }
  
  })

  
  app.get("/deleteRow/:courseID",(req,res)=>{
    const connection = mysql.createConnection({
      host: 'localhost',
      port: '3306',
      user: 'root',
      password: 'password',
      database: 'sorn_mysql'
    })
   
    const queryString = "SELECT * FROM teach WHERE courseID = '" + req.params.courseID + "'" 
    connection.query(queryString,(err,rows,fields)=>{
      if(err){
        console.log("Failed to query for users" + err)
        res.sendStatus(500)
        return
        //throw err
      }
      console.log("I think we fetched users successfully")
      const users = rows.map((row) => {
        return {t_id: row.t_id ,courseID: row.courseID, username: row.username,passwd: row.passwd ,state: row.state}
      })
      console.log("#################"+users)
      res.json(users)
      })
    if(req.params.condition == "no"){
      console.log("update course set state ='"+req.params.condition+"' where courseID =" +req.params.courseID )
    }
    
    })



app.get("/", (req,res ) => {
  console.log("Responding to root route")
  res.sendFile(path.join(__dirname, '../build', 'index.html'))
})




app.listen(3000,() => {
  console.log("Server is up and listening on 3000...")
})

