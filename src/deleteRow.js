import React from 'react'
import './CourseRow.css';
import $ from 'jquery'
import './sass/_loginSty.scss'
class deleteRow extends React.Component{
  constructor(props){
    super(props);
      this.state = {state:false
      };
      
    
}
    componentDidMount(){
        var string1 = ("yes").trim();
        var string2 = (this.props.course.state).trim();
        //console.log(string2)
        //console.log(string1==string2)
        if(string1===string2){
            this.setState({state:true})
            // console.log(this.state.tutor)
        }
    }
    handAccept(event){
      const urlString = "http://localhost:3000/confirm/paying/"+this.props.course.courseID
      $.ajax({
        method: 'GET',
        url: urlString,
        success: (searchResults) =>{
          console.log("Fetched data successfully" + urlString)
          console.log(searchResults)
          this.props.state = "tutorSearch"
        },
        error: (xhr , status , err) => {
          console.error("Failed to fetch data")
        }
      })
      this.props.handDel()

    }
    handDecline(event){
      const urlString = "http://localhost:3000/confirm/no/"+this.props.course.courseID
      $.ajax({
        method: 'GET',
        url: urlString,
        success: (searchResults) =>{
          console.log("Fetched data successfully" + urlString)
          console.log(searchResults)
          this.props.state = "tutorSearch"
        },
        error: (xhr , status , err) => {
          console.error("Failed to fetch data")
        }
      })
      this.props.handDel()
      
    }
 
    handClick(event){
        console.log("id is", event.target.value)
        var data = event.target.value
       const urlString = "http://localhost:3000/courseDel/"+data
       $.ajax({
         method: 'GET',
         url: urlString,
         success: (searchResults) =>{
           console.log("Fetched data successfully" + urlString)
           console.log(searchResults)
           this.props.state = "tutorSearch"
         },
         error: (xhr , status , err) => {
           console.error("Failed to fetch data")
         }
       })
       this.props.handDel()
  }
  
  handClickTutor(event){
    var username = ""
    var password = ""
    var arr2;
    const urlString = "http://localhost:3000/deleteRow/" + this.props.course.courseID
    console.log("========================"+urlString)
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        arr2=JSON.parse(arr1);
        //const results = searchResults.results
        // console.log("arr2 deleteRow requrest-------------------" +arr2[0])
        var courseRows =[]
        //console.log(courseRows)
        
        arr2.forEach((course) => {
          console.log("####################################### " +course.username + " " + course.passwd)
          this.props.handleTutorProfile(course.username,course.passwd)
          // courseRows.push(courseRow)
        })
        
        // this.setState({rows: courseRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })

    
  }
  //this.props.handleTutorProfile(username,password)
  

    render(){
        return<table className = "fixed" key={this.props.course.courseID} >

        <tbody>
            <tr className = "tr" >
                <td><img alt = "app_icon" width = "120" src="student.png"/></td>
               <td align ="center"> {this.props.course.subject}</td>
                    <td>{this.props.course.detail}</td>
                    <td>{this.props.course.day}</td>
                    <td>{this.props.course.time}</td>
                 
                    {!(this.state.state)&&<td><button type="button" className="btn" onClick={(e) => {
                      if(window.confirm('Delete the item?')) 
                      this.handClick(e)
                      
                      }} value = {this.props.course.courseID}>Delete</button> </td>
                    }
                      {this.state.state&& 
                        <td><button type="button" className="btn" onClick={(e) => {
                      if(window.confirm('Confirm this item?')) 
                      this.handAccept(e)
                      }} value = {this.props.course.courseID}>Accept</button></td>}
                      {this.state.state&& 
                        <td><button type="button" className="btn" onClick={(e) => {
                      if(window.confirm('Decline this item?')) 
                      this.handDecline(e)
                      
                      }} value = {this.props.course.courseID}>Decline</button></td>}
                       {this.state.state&& 
                        <td><button type="button" className="btn" value = {this.props.course.courseID} onClick={(e) => {
                          this.handClickTutor(e)
                          
                          }}>Tutor Profile</button></td>}
            </tr>
            </tbody>
        </table>
    }

}
export default deleteRow
 