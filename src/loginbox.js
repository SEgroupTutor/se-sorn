import './sass/_loginSty.scss'
import React from 'react'
import $ from 'jquery'

class LoginBox extends React.Component {

    constructor(props) {
      super(props);
      this.state = {valueusername:"",
                    valuepassword:"",
                  valuestatus:"student",
                submitted:false};

    this.handlePassword = this.handlePassword.bind(this);
    this.handleUsername = this.handleUsername.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleStatus = this.handleStatus.bind(this);
  
    }

    // submitLogin(e) {
    //     e.preventDefault()
    // }
    handleUsername(text)
    {
      this.setState({valueusername:text.target.value})
    }
    handlePassword(text)
    {
      this.setState({valuepassword:text.target.value})
    }
    handleSubmit = (event) =>
    {
      event.preventDefault();
      this.setState({submitted:true})
      //e.preventDefult();
      var u1 = this.state.valueusername
      var u2 = this.state.valuepassword
      var u3 = this.state.valuestatus
      this.props.handlerSubmit(u1,u2,u3)
    }/*
    handleSubmit(event){
        event.preventDefault();
      var data = {
       username: this.state.valueusername,
       password: this.state.valuepassword
   }
   console.log(data)
   fetch("http://localhost:3000/login", {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(data)
    }).then(function(response) {
      if (response.status == 500) {
        alert("Invalid username or password.")
        throw new Error("Bad response from server");
      }
      if (response.status == 501) {
        alert("Please enter username and password.")
        throw new Error("Bad response from server");
      }
      if (response.status == 399) {
        console.log("Welcome "+data.username)
      }
      return response.json();
  }).catch(function(err) {
      console.log(err)
  });
  }*/
  handleStatus=(event)=>{
    event.preventDefault();
    console.log(this.state.valuestatus)
    this.setState({valuestatus: event.target.value})
  }
    render() {
      return (
        <div className="inner-container">
          <div className="header">
            Login
          </div>
          <div className="box">
  
            <div className="input-group">
              <label htmlFor="username">Username</label>
              <input type="text" className="login-input" value={this.state.valueusername} onChange={this.handleUsername}/>
              {this.state.submitted && !this.state.valueusername &&
                <div className="help-block">Username is required</div>
              }
            </div>
  
            <div className="input-group">
              <label htmlFor="password">Password</label>
              <input type="password" className="login-input"value={this.state.valuepassword} onChange={this.handlePassword}/>
              {this.state.submitted && !this.state.valuepassword &&
                <div className="help-block">Username is required</div>
              }
            </div>
            <div className="input-group">
              <label htmlFor="type">Login as</label>
              <select name="" onChange = {this.handleStatus}className="select-type" value = {this.state.value}>
                <option value="student">Student</option>
                <option value="tutor">Tutor</option>
              </select>
            </div>
            
            <button type="button" className="login-btn" onClick={this.handleSubmit}>Login</button>
          </div>
        </div>
      );
    }
  
  }
  export default LoginBox