
import './sass/_loginSty.scss'
import React from 'react'
class RegisterBox extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
          valueUsername:'',
          valuePassword:'',
          valueFirstName:'',
          valueLastName:'',
          valueContact:'',
          valueStatus:'student',
        submitted: false

          
      };
      this.handlePassword = this.handlePassword.bind(this);
      this.handleUsername = this.handleUsername.bind(this);
      this.submitRegister = this.submitRegister.bind(this);
      this.handleFirstname = this.handleFirstname.bind(this);
      this.handleLastname = this.handleLastname.bind(this);
      this.handleContact = this.handleContact.bind(this);
      this.handleStatus = this.handleStatus.bind(this);
    }
  
    /*submitRegister(e) {
        e.preventDefault();

        alert(
            this.state.valueUsername + "\n" 
            + this.state.valuePassword + "\n"
            + this.state.valueFirstName + "\n"
            + this.state.valueLastName + "\n"
            + this.state.valueContact+"\n"
            +this.state.valueStatus
        )
        console.log("username ---->"+typeof(this.state.username))
        if (this.state.username.trim() == ""|| this.state.username==undefined) {
          this.showValidationErr("username", "Username Cannot be empty!");
          alert("Username Cannot be empty!")
        }
        if (this.state.password.trim() == ""|| this.state.valuePassword==undefined) {
          this.showValidationErr("password", "Password Cannot be empty!");
        }
    }*/
    handleUsername(event){
        this.setState({valueUsername: event.target.value})
        event.preventDefault();
       // this.clearValidationErr("username");
    }
    handlePassword(event){
        event.preventDefault();
        this.setState({valuePassword: event.target.value})
        //  this.clearValidationErr("password");
    }
    handleFirstname(event){
        event.preventDefault();
        this.setState({valueFirstName: event.target.value})
    }
    handleLastname(event){
        event.preventDefault();
        this.setState({valueLastName: event.target.value})
    }
    handleContact(event){
        event.preventDefault();
        this.setState({valueContact: event.target.value})
    }

    submitRegister(event){
      event.preventDefault();
      this.setState({submitted:true});
      var data = { status : this.state.valueStatus ,
        username: this.state.valueUsername,
        password: this.state.valuePassword,
        firstname: this.state.valueFirstName,
        lastname: this.state.valueLastName,
        contact: this.state.valueContact
      }
      console.log(data)
      if(data.status && data.username && data.password && data.firstname && data.lastname && data.contact){
        fetch("http://localhost:3000/register", {
          method: 'POST',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify(data)
        }).then((response)=> {
          console.log(response.status)
          if (response.status == 500) {
            alert("Please fill infomation")
            throw new Error("Bad response from server");
          } if (response.status == 501){
            alert("Please fill infomation")
            throw new Error("Bad response from server");
          } if(response.status == 203){
            alert("Username already used")
            throw new Error("Bad response from server");

          }
          if(response.status == 201) {
            console.log("Reg fin")
            this.props.handleReg(this.state.valueUsername,this.state.valuePassword,this.state.valueStatus)
          }
            return response.json()
        }).catch(function(err) {
          console.log(err)
  });
      }else{
        alert("Please fill all the information needed!")
      }
   
}
    handleStatus(event){
      event.preventDefault();
      this.setState({valueStatus: event.target.value})
      console.log(this.state.valueStatus)
    }


    //Add New Error Object to the array {elm: msg}
  /*  showValidationErr(elm, msg) {
      this.setState((prevState) => ({
        errors: [
          ...prevState.errors, {
            elm,
            msg
          }
        ]
      }));
    }*/
    //Remove a specific element from the array 
    /*clearValidationErr(elm) {
      this.setState((prevState) => {
        let newArr = [];
        //Add all elements from the prev array to the new one that has a different element
        for (let err of prevState.errors) {
          if (elm != err.elm) {
            newArr.push(err);
          }
        }
        return {errors: newArr};
      });
    }
*/


    render() {
      const { registering  } = this.props;
      const { valueUsername,valuePassword,valueFirstName,valueLastName,valueContact,valueStatus,submitted} = this.state;
      return (
        <div className="inner-container">
          <div className="header">
            Register
          </div>
          <div className="input-group">
              <label htmlFor="type" className="label1">Register as</label>
              <select name="" onChange={this.handleStatus} className="select-type" value ={this.state.value}>
                <option value="student">Student</option>
                <option value="tutor">Tutor</option>
              </select>
            </div>
          <div className="box">
  
            <div className="input-group">
              <label htmlFor="username">Username</label>
              <input type="text" value={this.state.valueUsername} name="username" className="login-input" placeholder="Username" onChange={this.handleUsername}/>
              {submitted && !valueUsername &&
                <div className="help-block">Username is required</div>
              }
              
            </div>

            <div className="input-group">
              <label htmlFor="password">Password</label>
              <input type="password" value={this.state.valuePassword} name="password" className="login-input" placeholder="Password" onChange={this.handlePassword}/>
              {submitted && !valuePassword &&
                <div className="help-block">Password is required</div>
              }
            </div>

            <div className="input-group">
              <label htmlFor="username">Firstname</label>
              <input type="text" value={this.state.valueFirstName} name="firstname" className="login-input" placeholder="Firstname" onChange={this.handleFirstname}/>
              {submitted && !valueFirstName &&
                <div className="help-block">Firstname is required</div>
              }
            </div>

            <div className="input-group">
              <label htmlFor="username">Lastname</label>
              <input type="text" value={this.state.valueLastName} name="lastname" className="login-input" placeholder="Lastname" onChange={this.handleLastname}/>
              {submitted && !valueLastName &&
                <div className="help-block">Lastname is required</div>
              }
            </div>
            <div className="input-group">
              <label htmlFor="username">Contact</label>
              <input type="text" value={this.state.valueContact} name="contact" className="login-input" placeholder="Contact" onChange={this.handleContact}/>
              {submitted && !valueContact &&
                <div className="help-block">Contact is required</div>
              }
            </div>
            


            <button type="button" className="login-btn" onClick={this.submitRegister}>Register</button>
          </div>
        </div>
      );
    }
  }
  export default RegisterBox