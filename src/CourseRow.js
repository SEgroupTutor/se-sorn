import React from 'react'
import './CourseRow.css';
import './sass/_loginSty.scss'
class CourseRow extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            tutor:false
        }
        this.handle1Teach = this.handle1Teach.bind(this)
        
        
      }
      handle1Teach(){
          console.log("teach")
          this.props.handleTeach(this.props.course.courseID)
      }
      componentDidMount(){
        var string1 = ("tutor").trim();
        var string2 = (this.props.status).trim();
        // console.log(string1==string2)
        if(string1==string2){
            this.setState({tutor:true})
            // console.log(this.state.tutor)
        }
    }
    


    
    render(){
        return<table className = "fixed" key={this.props.course.courseID}  >
        <tbody>
            <tr className = "tr" >
                <td><img alt = "app_icon" width = "120" src="student.png"/></td>
               <td align ="center"> {this.props.course.subject}</td>
                    <td>{this.props.course.detail}</td>
                    <td>{this.props.course.day}</td>
                    <td>{this.props.course.time}</td>  
                    {this.state.tutor&& 
                     <td><button type="button" className="btn" onClick={(e) => {
                        if(window.confirm('Teach this course?')) 
                        this.handle1Teach(e)
                        
                        }} value = {this.props.course.courseID}>Teach</button></td>
                    }

                   

                    
            </tr>
            </tbody>
        </table>
        
    }

}
export default CourseRow
 