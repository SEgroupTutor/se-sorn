import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './App.css';


import CourseRow from './CourseRow.js'
import DeleteRow from './deleteRow.js'
import MatchRow from './matchRow.js';

import $ from 'jquery'
import StudentPost from './StudentPost';
import './sass/_loginSty.scss'
import LoginBox from './loginbox'
import RegisterBox from './RegisterBox'
import HisRow from './HisRow.js';
import UserProfile from "./UserProfile"




//statePage : 
// "tutorSearch" 
// "deleteSearch" admin delete course
// "studentPost" student post couse
// "MainPage""
// "History"
// "Profile"
// "courseDetail" tutor click teach
// "viewProfile"

//import { slide as Menu } from 'react-burger-menu'
class App extends Component {
  constructor(props){
    super(props)

    this.state={statePage:"MainPage", 
    username:"",
    password:"",
    status: "student",
    isLoginOpen:true,
    isRegisterOpen:false,
    firstname:null,
    lastname:null,
    contact:null}



    var data1 =""
    var data2 ="day"
    var data3 = "time"
    
    //handle login button
    this.handlerSubmit = this.handlerSubmit.bind(this)
    this.handleReg = this.handleReg.bind(this)
    this.handleHistory = this.handleHistory.bind(this)
    this.handleLogout = this.handleLogout.bind(this)
    this.handleProfile = this.handleProfile.bind(this)
    this.handlePost = this.handlePost.bind(this)
    this.handlePostSubmit = this.handlePostSubmit.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handDel = this.handDel.bind(this)
    this.handleTeach = this.handleTeach.bind(this)
    this.handleTutorProfile = this.handleTutorProfile.bind(this)

    if(this.state.statePage === "tutorSearch"){
      console.log("from login-> serach")
      this.performSearch(data1,data2,data3)
    }
    if(this.state.statePage === "deleteSearch"){
      this.DeleteSearch(data1,data2,data3)
    }
    // if(this.state.statePage === "studentPost"){
    //   this.DeleteStudentSearch(this.state.username,this.state.password)
    // }

    if(this.state.statePage === "History" && this.state.status === "student"){
      this.DeleteStudentSearch(this.state.username,this.state.password)
    }
    if(this.state.statePage === "History" && this.state.status === "tutor"){
      this.tutorHistory(this.state.username,this.state.password)
    }
    // console.log(this.state.statePage)
  }
  tutorHistory(username , password){
    console.log("tutorHis")
    const urlString = "http://localhost:3000/teachby/"+username+"/"+password
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully " + urlString)
        console.log(searchResults)
       // this.setState({srows: null})
       this.setState({hrows: null})
        var arr1=JSON.stringify(searchResults);
        var arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0])
        var HisRows =[]
        //console.log(courseRows)
        arr2.forEach((course) => {
          const hisRow = <HisRow key={course.courseID} course ={course} handDel={this.handDel}  />
          HisRows.push(hisRow)
        })
        console.log(HisRows)
        this.setState({hrows: HisRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })

  }

  DeleteStudentSearch(searchTerm1,searchTerm2) {
    //searchTerm == SID ใช้หา course history
    this.setState({srows: null})
    this.setState({mrows: null})
    console.log("Perform search using api")
    console.log(searchTerm1)
    const urlString = "http://localhost:3000/course12/"+searchTerm1+"/"+searchTerm2
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully " + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        var arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0])
        var deleteRows =[]
        var matchRows = []
        //console.log(courseRows)
        arr2.forEach((course) => {
          //console.log(course.first_name)
          const deleteRow = <DeleteRow key={course.courseID} course ={course} handDel={this.handDel} handleTutorProfile={this.handleTutorProfile}/>
          const matchRow = <MatchRow key = {course.courseID } course ={course} handDel={this.handDel}/>
          this.setState({

          })
          console.log(course.state == "payed")
          if(course.state == "payed"){
          matchRows.push(matchRow)}else{
            deleteRows.push(deleteRow)
          }
        })
        console.log(matchRows)
        this.setState({srows: deleteRows})
        this.setState({mrows: matchRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
  }


  DeleteSearch(searchTerm1,searchTerm2,searchTerm3) {
    
    console.log("Perform search using api")
    console.log(searchTerm1)
    const urlString = "http://localhost:3000/course/"+searchTerm1
    $.ajax({

      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        var arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0])
        var deleteRows =[]
        //console.log(courseRows)
        arr2.forEach((course) => {
          //console.log(course.first_name)
          const deleteRow = <DeleteRow key={course.courseID} course ={course} handDel = {this.handDel}/>
          deleteRows.push(deleteRow)
        })
        this.setState({drows: deleteRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
  }

  

  performSearch(searchTerm1,searchTerm2,searchTerm3) {
    
    console.log("Perform search using api")
    console.log(searchTerm1)
    const urlString = "http://localhost:3000/course/"+searchTerm1
    $.ajax({
      
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        this.setState({rows: null})
        var arr1=JSON.stringify(searchResults);
        var arr2=JSON.parse(arr1);
        //const results = searchResults.results
        // console.log(arr2[0])
        var courseRows =[]
        //console.log(courseRows)
        arr2.forEach((course) => {
          // console.log(course)
          //console.log(course.first_name)


          const courseRow = <CourseRow key={course.courseID} course ={course} status={this.state.status} handleTeach = {this.handleTeach}/>

          console.log("--------------------------------"+this.state.status)
          if(course.state != "payed" && course.state != "paying"){
          courseRows.push(courseRow)}
        })
   
        this.setState({rows: courseRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
  }


  performFilter(searchTerm1,searchTerm2,searchTerm3) {
    var urlString;
    console.log("Perform search using api")
    console.log("searchTerm2 " + searchTerm2)
    console.log("searchTerm1 " + searchTerm1)
    console.log("searchTerm3 " + searchTerm3)
    console.log(typeof searchTerm1 == 'undefined')
    console.log()
      urlString = "http://localhost:3000/course/" +searchTerm1 + "/filter/"+searchTerm2+"/"+searchTerm3
    console.log(urlString)
    
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        var arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0])
        var courseRows =[]
        //console.log(courseRows)
        arr2.forEach((course) => {
          //console.log(course.first_name)
          const courseRow = <CourseRow key={course.courseID} course ={course} status={this.state.status} handleTeach= {this.handleTeach}/>
          courseRows.push(courseRow)
        })
   
        this.setState({rows: courseRows})
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
  }

  searchChangeHandler(event){
    console.log(event.target.value)
    const boundObject = this
    const searchTerm = event.target.value
    this.data1 = searchTerm.toString()
    boundObject.performSearch(this.data1,boundObject.data2,boundObject.data3)
  }
  selectDayChangeHandler(event){
    console.log(event.target.value)
    const boundObject = this
    const selectTerm = event.target.value
    this.data2 = selectTerm
    boundObject.performFilter(this.data1,this.data2,this.data3)
  }
  selectTimeChangeHandler(event){
    const boundObject = this
    const searchTerm = event.target.value
    this.data3 = searchTerm
    boundObject.performFilter(this.data1,this.data2,this.data3)
  }

  showLoginBox() {
    this.setState({isLoginOpen: true, isRegisterOpen: false});
  }

  showRegisterBox() {
    this.setState({isRegisterOpen: true, isLoginOpen: false});
  }

  //handle login button
  handlerSubmit = (valueusername, valuepassword , valuestatus)=>{
      var data = {
       username: valueusername,
       password: valuepassword,
       status: valuestatus
   }
   console.log(data)
   fetch("http://localhost:3000/login", {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(data)
    }).then((response)=> {
      console.log(response.status)
      if (response.status == 500) {
        alert("Invalid username or password.")
        throw new Error("Bad response from server");
      }
      if (response.status == 501) {
        alert("Please enter username and password.")
        throw new Error("Bad response from server");
      }
      if (response.status == 399) {
        console.log("Welcome "+data.username)
  this.setState({
    statePage: "tutorSearch",
    username: valueusername,
    password: valuepassword,
    status: valuestatus,
    otherfirstname:"",
          otherlastname: "",
          othercontact:""
    
  }) 
        return response.json()
      }
    })
  
  .catch(function(err) {console.log(err)});
        /*if(this.state.statePage!="studentPost"){
          this.setState({
            statePage: "tutorSearch"
          })
        }*/
        this.performSearch(" ","","")
  }

  handlePostSubmit=(subject, day , time,student,price,period,place,detail)=>{
    var data = {
      student: student,
      subject: subject,
      day: day,
      detail: detail,
      time: time,
      price: price,
      place: place,
      period: period,
      username: this.state.username,
      password : this.state.password
  }
    console.log(data)
    fetch("http://localhost:3000/courses", {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(data)
    }).then((response)=> {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
  
        }else {
          console.log(response.status)
          this.setState({statePage:"tutorSearch"});
        }
        return response.json();
    }).catch(function(err) {
        console.log(err)
    });


  }
  handlePost(){
    this.setState({statePage:"studentPost",otherfirstname:"",
    otherlastname: "",
    othercontact:""})}

  handleHistory(){

    this.setState({
      statePage:"History",
      otherfirstname:"",
          otherlastname: "",
          othercontact:""
    })
    if(this.state.status === "student"){
      this.DeleteStudentSearch(this.state.username,this.state.password)
    }
    if(this.state.status === "tutor"){
      this.tutorHistory(this.state.username,this.state.password)
    }

    //this.DeleteStudentSearch(this.state.username, this.state.password);


  }
  handleLogout(){
    this.setState({
      statePage: "MainPage",
      username: "",
      password: "",
      status: "",
      firstname:"",
          lastname: "",
          contact:"",
          otherfirstname:"",
          otherlastname: "",
          othercontact:""
    })
  }
  handleProfile(){
    const urlString = "http://localhost:3000/profile/" +this.state.username + "/"+this.state.password
    var arr2;
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0])
        var courseRows =[]
        //console.log(courseRows)
        console.log("arr2 profile" + arr2[0].firstname)
        this.setState({
          statePage:"Profile",
          firstname:arr2[0].firstname,
          lastname: arr2[0].lastname,
          contact:arr2[0].contact,
          otherfirstname:"",
          otherlastname: "",
          othercontact:""
        })
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
    
    console.log(this.state.firstname + this.state.lastname+this.state.contact)

  }
  handleSearch(){
    this.setState({
      statePage:"tutorSearch",
      otherfirstname:"",
          otherlastname: "",
          othercontact:""
    })
    this.performSearch("","","")
  }
  

  handleReg(username,password,status){
    this.setState({statePage: "tutorSearch",
  username : username,
password: password,
status: status})
  }

  handDel(){
    this.setState({
      statePage:"tutorSearch"
    })
  }

  handleTeach=(courseID)=>{
    var data = {courseID:courseID,
    username: this.state.username,
  password: this.state.password}
    console.log("select course: "+ courseID )
    fetch("http://localhost:3000/teach", {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(data)
  }).then((response)=> {
      if (response.status >= 400) {
        throw new Error("Bad response from server");

      }else {
        console.log("teach insert")
        this.setState({statePage:"tutorSearch"});
      }
      return response.json();
  }).catch(function(err) {
      console.log(err)
  });


  }

  handleTutorProfile(username,password){
    const urlString = "http://localhost:3000/profile/" +username + "/"+password
    var arr2;
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        var arr1=JSON.stringify(searchResults);
        arr2=JSON.parse(arr1);
        //const results = searchResults.results
        console.log(arr2[0]);


        this.setState({
          statePage:"viewProfile",
          otherfirstname:arr2[0].firstname,
          otherlastname: arr2[0].lastname,
          othercontact:arr2[0].contact
        })


      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
  }





  render() {
    
    if(this.state.statePage==="MainPage"){
      return (
        <div className="root-container" bgcolor="#f3a8d6">
              <div className="box-controller">
                <div
                  className={"controller " + (this.state.isLoginOpen
                  ? "selected-controller"
                  : "")}
                  onClick={this
                  .showLoginBox
                  .bind(this)}>
                  Login
                </div>
                <div
                  className={"controller " + (this.state.isRegisterOpen
                  ? "selected-controller"
                  : "")}
                  onClick={this
                  .showRegisterBox
                  .bind(this)}>
                  Register
                </div>
                
              </div>
          <div className="box-container" bgcolor="#fff">
            {this.state.isLoginOpen && <LoginBox handlerSubmit = {this.handlerSubmit} />}
            {this.state.isRegisterOpen && <RegisterBox handleReg = {this.handleReg} />}
          </div>
        </div>
        );
    }
    if(this.state.statePage === "tutorSearch" && this.state.status === "student" ){
      return (
        <div className="App">
          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>Search</h2>
                <td>                           </td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td>
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
            </tr>  
          </table>
          <table width="100%">
          <input class= "search-input" onChange = {this.searchChangeHandler.bind(this)} placeholder = "Enter search "/>
          </table>
          
          <table align="center" width="100%">
          <tr>
            <th>วัน</th> 
            <th><select name="Var" onChange={this.selectDayChangeHandler.bind(this)} value={this.state.value}>
              <option> เลือกวัน </option>
              <option value="mon">mon</option>
              <option value="tue">tue</option>
              <option value="wed">wed</option>
              <option value="thu">thu</option>
              <option value="fri">fri</option>
              <option value="sat">sat</option>
              <option value="sun">sun</option>
            </select>
            </th>
            <th>เวลา</th>
            <th>
              <select name="Var" onChange={this.selectTimeChangeHandler.bind(this)} value={this.state.value}>
              <option> ช่วงเวลา </option>
              <option value="เช้า">เช้า</option>
              <option value="เที่ยง">เที่ยง</option>
              <option value="บ่าย">บ่าย</option>
              <option value="เย็น">เย็น</option>
            </select>
            </th>
            
          </tr>
          </table>
          {this.state.rows}
        </div>
      );
    }
    if(this.state.statePage === "tutorSearch" && this.state.status === "tutor" ){
      return (
        <div className="App">
          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>Search</h2>
                <td>                           </td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
            </tr>  
          </table>
          <table width="100%">
          <input class= "search-input" onChange = {this.searchChangeHandler.bind(this)} placeholder = "Enter search "/>
          </table>
          <table align="center" width="100%">
          <tr>
            <th>วัน</th> 
            <th><select name="Var" onChange={this.selectDayChangeHandler.bind(this)} value={this.state.value}>
              <option> เลือกวัน </option>
              <option value="mon">mon</option>
              <option value="tue">tue</option>
              <option value="wed">wed</option>
              <option value="thu">thu</option>
              <option value="fri">fri</option>
              <option value="sat">sat</option>
              <option value="sun">sun</option>
            </select>
            </th>
            <th>เวลา</th>
            <th>
              <select name="Var" onChange={this.selectTimeChangeHandler.bind(this)} value={this.state.value}>
              <option> ช่วงเวลา </option>
              <option value="เช้า">เช้า</option>
              <option value="เที่ยง">เที่ยง</option>
              <option value="บ่าย">บ่าย</option>
              <option value="เย็น">เย็น</option>
            </select>
            </th>
            
          </tr>
          </table>
          {this.state.rows}
        </div>
      );
    }
    if(this.state.statePage === "deleteSearch"){
      return (
        <div className="App">
          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>Admin delete course</h2>
                <td width="60%">                           </td>
                {/* <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>Post couse</button></td> */}
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
            </tr>  
          </table>
          <input class="search-input" onChange = {this.searchChangeHandler.bind(this)} placeholder = "Enter search "/>
          <table align="center" width="100%">
            <tr>
              <th>วัน </th><th><select name="Var" onChange={this.selectDayChangeHandler.bind(this)} value={this.state.value}>
                <option> เลือกวัน </option>
                <option value="mon">mon</option>
                <option value="tue">tue</option>
                <option value="wed">wed</option>
                <option value="thu">thu</option>
                <option value="fri">fri</option>
                <option value="sat">sat</option>
                <option value="sun">sun</option>
              </select></th>
              <th>เวลา </th><th><select name="Var" onChange={this.selectTimeChangeHandler.bind(this)} value={this.state.value}>
                <option> ช่วงเวลา </option>
                <option value="เช้า">เช้า</option>
                <option value="เที่ยง">เที่ยง</option>
                <option value="บ่าย">บ่าย</option>
                <option value="เย็น">เย็น</option>
              </select></th>
            </tr>
          </table>
          {this.state.drows}
        </div>
      );
    }
    if(this.state.statePage === "studentPost" && this.state.status === "student" ){
      return (
        <div className="App">

          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>Post</h2>
                <td>                           </td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td>
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
            </tr>  
          </table>
          <StudentPost handlePostSubmit = {this.handlePostSubmit}/>;
        </div>
      );
    } 
    if(this.state.statePage==="History" && this.state.status === "student" ){
      return(
        <div className="App">
          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>My History</h2>
                <td>                           </td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td>
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
            </tr>  
          </table>
          {this.state.srows}

          <table width ="100%" align="left"><h2>Confirm matched course</h2></table>
          {this.state.mrows}
        </div>
       
      );
    }
    if(this.state.statePage==="History" && this.state.status === "tutor" ){
      return(
        <div className="App">
          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>My History</h2>
                <td>                           </td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                {/* <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td> */}
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
            </tr>  
          </table>
          {this.state.hrows}


        </div>
      );
    }
    if(this.state.statePage === "Profile" && this.state.status === "student" ){
      return (
        <div className="App">
          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>My Profile</h2>
                <td>                           </td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td>
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
                
            </tr>  
          </table>
          {/* <h2>Profile data asdl;kfkasdfkals;dfkla;sfk;lsdkf;</h2> */}
          <UserProfile firstname={this.state.firstname} lastname={this.state.lastname} contact={this.state.contact}status={this.state.status}/>
        </div>
       
      );
    }
    if(this.state.statePage === "Profile" && this.state.status === "tutor" ){
      // console.log(this.state.status)
      return (
        <div className="App">
          <table align = "left" bgcolor="#f3a8d6" width="100%">
            <tr className="tutorHeader">
                <img alt = "app_icon" width = "120" src="logo.png"/>
                <h2>My Profile</h2>
                <td>                           </td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
                <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
                {/* <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td> */}
                <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
            </tr>  
          </table>
          {/* <h2>Profile data asdl;kfkasdfkals;dfkla;sfk;lsdkf;</h2> */}
          <UserProfile firstname={this.state.firstname} lastname={this.state.lastname} contact={this.state.contact} status={this.state.status}/>
        </div>
       
      );
    }
    if(this.state.statePage === "courseDetail" && this.state.status === "tutor" ){
      return(<div className="App">
      <table align = "left" bgcolor="#f3a8d6" width="100%">
        <tr className="tutorHeader">
            <img alt = "app_icon" width = "120" src="logo.png"/>
            <h2>Course Detail</h2>
            <td>                           </td>
            <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
            <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
            <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
            {/* <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td> */}
            <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
        </tr>  
      </table>
      {/* <h2>Profile data asdl;kfkasdfkals;dfkla;sfk;lsdkf;</h2> */}
    </div>);

    }
    if(this.state.statePage==="viewProfile" && this.state.status==="student"){
      return(<div className="App">
      <table align = "left" bgcolor="#f3a8d6" width="100%">
        <tr className="tutorHeader">
            <img alt = "app_icon" width = "120" src="logo.png"/>
            <h2>tutor Detail</h2>
            <td>                           </td>
            <td><button type="button" align = "right" className="nav-btn" onClick={this.handleSearch}>Search</button></td>
            <td><button type="button" align = "right" className="nav-btn" onClick={this.handleProfile}>Profile</button></td>
            <td><button type="button" align = "right" className="nav-btn" onClick={this.handleHistory}>History</button></td>
            <td><button type="button" align = "right" className="nav-btn" onClick={this.handlePost}>Post couse</button></td>
            <td> <button type="button" align = "right" className="nav-btn" onClick={this.handleLogout}>Logout</button></td>
        </tr>  
      </table>
      <UserProfile firstname={this.state.otherfirstname} lastname={this.state.otherlastname} contact={this.state.othercontact}status={this.state.otherstatus}/>
    </div>

      );
    }
  }
}
ReactDOM.render(
  <App/>,
  document.getElementById('root')
);
export default App;


