import React from 'react'
import './CourseRow.css';
import $ from 'jquery'
import './sass/_loginSty.scss'
class mathRow extends React.Component{
  constructor(props){
    super(props);
      this.state = {state:false
      };
      
    
}
    componentDidMount(){
        var string1 = ("payed").trim();
        var string2 = (this.props.course.state).trim();
        console.log(string2)
        console.log(string1==string2)
        if(string1===string2){
            this.setState({state:true})
            // console.log(this.state.tutor)
        }
    }
    handAccept(event){
      const urlString = "http://localhost:3000/confirm/paying/"+this.props.course.courseID
      $.ajax({
        method: 'GET',
        url: urlString,
        success: (searchResults) =>{
          console.log("Fetched data successfully" + urlString)
          console.log(searchResults)
          this.props.state = "tutorSearch"
        },
        error: (xhr , status , err) => {
          console.error("Failed to fetch data")
        }
      })
      this.props.handDel()

    }
    handDecline(event){
      const urlString = "http://localhost:3000/confirm/no/"+this.props.course.courseID
      $.ajax({
        method: 'GET',
        url: urlString,
        success: (searchResults) =>{
          console.log("Fetched data successfully" + urlString)
          console.log(searchResults)
          this.props.state = "tutorSearch"
        },
        error: (xhr , status , err) => {
          console.error("Failed to fetch data")
        }
      })
      this.props.handDel()
      
    }
 
    handClick(event){
        console.log("id is", event.target.value)
        var data = event.target.value
       const urlString = "http://localhost:3000/courseDel/"+data
       $.ajax({
         method: 'GET',
         url: urlString,
         success: (searchResults) =>{
           console.log("Fetched data successfully" + urlString)
           console.log(searchResults)
           this.props.state = "tutorSearch"
         },
         error: (xhr , status , err) => {
           console.error("Failed to fetch data")
         }
       })
       this.props.handDel()
  }

    render(){
        return(<table className = "fixed" key={this.props.course.courseID} >

        <tbody>
            <tr className = "tr" >
            
                <td><img alt = "app_icon" width = "120" src="student.png"/></td>
               <td align ="center"> {this.props.course.subject}</td>
                    <td>{this.props.course.detail}</td>
                    <td>{this.props.course.day}</td>
                    <td>{this.props.course.time}</td>
            
                    
            </tr>
            </tbody>
        </table>);
        
    }

}
export default mathRow
 