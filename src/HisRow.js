import React from 'react'
import './CourseRow.css';
import $ from 'jquery'
import './sass/_loginSty.scss'
class HisRow extends React.Component{
    constructor(props){
    super(props);
      this.state = {state:false,
        payed:false
      };
      
    
}
    componentDidMount(){
        var string1 = ("paying").trim();
        var string3 = ("payed").trim();
        var string2 = (this.props.course.state).trim();
        console.log(string2)
        console.log(string1==string2)
        if(string1===string2){
            this.setState({state:true})
            // console.log(this.state.tutor)
        }if(string3 === string2){
            this.setState({payed:true})
        }
    }

    
 
    handClick(event){
        console.log("id is", event.target.value)
        var data = event.target.value
       const urlString = "http://localhost:3000/courseDel/"+data
       $.ajax({
         method: 'GET',
         url: urlString,
         success: (searchResults) =>{
           console.log("Fetched data successfully" + urlString)
           console.log(searchResults)
           this.props.state = "tutorSearch"
         },
         error: (xhr , status , err) => {
           console.error("Failed to fetch data")
         }
       })
       this.props.handDel()
  }
  handPay(event){
    const urlString = "http://localhost:3000/confirm/payed/"+this.props.course.courseID
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        this.props.state = "tutorSearch"
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
    this.props.handDel()
  }
  handDecline(event){
    const urlString = "http://localhost:3000/confirm/no/"+this.props.course.courseID
    $.ajax({
      method: 'GET',
      url: urlString,
      success: (searchResults) =>{
        console.log("Fetched data successfully" + urlString)
        console.log(searchResults)
        this.props.state = "tutorSearch"
      },
      error: (xhr , status , err) => {
        console.error("Failed to fetch data")
      }
    })
    this.props.handDel()
    
  }



    render(){
        return<table className = "fixed" key={this.props.course.courseID} >

        <tbody>
            <tr className = "tr" >
                <td><img alt = "app_icon" width = "120" src="student.png"/></td>
               <td align ="center"> {this.props.course.subject}</td>
                    <td>{this.props.course.detail}</td>
                    <td>{this.props.course.day}</td>
                    <td>{this.props.course.time}</td>
                        {this.state.state&& 
                        <td><button type="button" className="btn" onClick={(e) => {
                      if(window.confirm('Pay the item?')) 
                      this.handPay(e)
                      }} value = {this.props.course.courseID}>Paying</button></td>}
                        {this.state.state&& 
                      <td><button type="button" className="btn" onClick={(e) => {
                        if(window.confirm('Pay the item?')) 
                        this.handDecline(e)
                        
                        }} value = {this.props.course.courseID}>Cancle</button></td>}
                       { !(this.state.state)&&!(this.state.payed)&&<td><button type="button" className="btn" value = {this.props.course.courseID}>Waiting</button></td>}


            </tr>
            </tbody>
        </table>
    }

}
export default HisRow
 