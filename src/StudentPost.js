import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './CourseRow.css';

 class StudentPost extends Component{
   constructor(props){
     super(props);
     this.state={
       statePage:"studenPost",
       valueSubject:"",
       valuePrice:"",
       valueDay:"",
       valueTime:"",
       valueDetail:"",
       valueStudent:"",
       valuePeriod:"",
       valuePlace:""
    };
    this.handleSubject = this.handleSubject.bind(this);
     this.handleChange = this.handleChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
     this.handleSelectDay = this.handleSelectDay.bind(this);
     this.handleSelectTime = this.handleSelectTime.bind(this);
     this.handleDetail = this.handleDetail.bind(this);
     this.handlePeriod = this.handlePeriod.bind(this);
     this.handleStudent = this.handleStudent.bind(this);
     this.handlePlace = this.handlePlace.bind(this);
     

   }
   handleChange(event){
     this.setState({valuePrice: event.target.value})
     event.preventDefault();
   }
   handleSubmit(event){
     alert("Detail was submitted: \n ราคา : "+this.state.valuePrice
     +"\n วิชา : "+this.state.valueSubject+  
     "\n วัน : "+this.state.valueDay+ 
     "\n เวลา : "+ this.state.valueTime+
     "\n จำนวนนักเรียน : "+ this.state.valueStudent+
     "\n ระยะเวลาเรียน : "+this.state.valuePeriod+
     "\nรายละเอียด : "+ this.state.valueDetail +
     "\n สถานที่สอน : " + this.state.valuePlace);
     event.preventDefault();

    this.props.handlePostSubmit(this.state.valueSubject ,this.state.valueDay , this.state.valueTime,this.state.valueStudent,this.state.valuePrice,this.state.valuePeriod,this.state.valuePlace,this.state.valueDetail)
}
   handleSelectDay(event){
    //alert(event.target.value)
     this.setState({valueDay: event.target.value})
   }
   handleSelectTime(event){
     //alert(event.target.value)
     this.setState({valueTime: event.target.value})
   }
   handleDetail(event){
     this.setState({valueDetail: event.target.value})
   }
   handleSubject(event){
    this.setState({valueSubject: event.target.value})
  }
   handleStudent(event){
     this.setState({valueStudent: event.target.value})
   }
   handlePeriod(event){
     this.setState({valuePeriod: event.target.value})
   }
   handlePlace(event){
     this.setState({valuePlace: event.target.value})
   }

     render(){
         return(
            <div className="App">
              <div className="PostField">
              <table className = "fixed"width="100%">
                <tr>
                <td width="25%">
                  <img alt = "app_icon" width = "120" src="student.png"/>
                </td>
                <td className="fillDetail" width="75%">
                  <form onSubmit={this.handleSubmit}>
                  <table width="100%">
                  <tr>
                      <th align="left" >
                          <td>วิชา</td> <td><input type="text" className="Spost"value={this.state.valueSubject} onChange={this.handleSubject}/></td>
                        </th>
                        <th align="left">
                          <td>สถานที่สอน</td> 
                          <td><input type="text"className="Spost" value={this.state.valuePlace} onChange={this.handlePlace}/></td>
                        </th>
                        <th align="left"><td>วัน</td> <td width="100%">
                        <select className="Sselect" name="Var" value={this.state.valueDay} onChange={this.handleSelectDay}>
                          <option> เลือกวัน </option>
                          <option value="mon">mon</option>
                          <option value="tue">tue</option>
                          <option value="wed">wed</option>
                          <option value="thu">thu</option>
                          <option value="fri">fri</option>
                          <option value="sat">sat</option>
                          <option value="sun">sun</option>
                        </select>
                          </td></th>
                        <th align="left"><td>
                        เวลา
                        </td>
                           <td width="100%">
                             <select name="Var" className="Sselect"value={this.state.valueTime} onChange={this.handleSelectTime}>
                          <option> ช่วงเวลา </option>
                          <option value="เช้า">เช้า</option>
                          <option value="เที่ยง">เที่ยง</option>
                          <option value="บ่าย">บ่าย</option>
                          <option value="เย็น">เย็น</option>
                        </select>
                             </td>
                        </th>
                  </tr>
                  <tr>
                      <th align="left">
                          
                          <td>ราคา
                            </td> <td><input type="text" className="Spost" value={this.state.valuePrice} onChange={this.handleChange}/></td>
                              
                        </th>
                        <th align="left">
                        <td>
                        เวลาการสอน
                        </td>
                        <td width="100%"><select name="Var"className="Sselect" value={this.state.valuePeriod} onChange={this.handlePeriod}>
                          <option> เรียนกี่ชั่วโมง </option>
                          <option value="1">2 ชั่วโมง</option>
                          <option value="2">2.30 ชั่วโมง</option>
                          <option value="3">3 ชั่วโมง</option>
                          <option value="4">4 ชั่วโมง</option>
                        </select>
                        </td>
                           
                          
                        </th>
                        <th align="left">
                        <td>
                        จำนวนนักเรียน
                        </td>
                           <td width="100%">
                           <select name="Var" className="Sselect"value={this.state.valueStudent} onChange={this.handleStudent}>
                          <option> จำนวนนักเรียน </option>
                          <option value="1">1 คน</option>
                          <option value="2">2 คน</option>
                          <option value="3">3 คน</option>
                          <option value="4">4 คน</option>
                          <option value="5">5 คน</option>
                        </select>
                           </td>
                          
                        </th>
                  </tr>
                      </table>
                      <div class="App-textarea ">
                        <textarea row = "20" align='center' data-resizable="true" 
                        placeholder="Please, insert your detail." maxLength="500" value={this.state.valueDetail} onChange={this.handleDetail}></textarea>
                      </div>
                      <input type="submit" align="right" value="Submit"/>
                  </form>
                  </td>
                </tr>

              </table>
              
            </div>
          </div>
         );
     }
 }

 export default StudentPost