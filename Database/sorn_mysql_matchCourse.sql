-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: sorn_mysql
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `matchCourse`
--

DROP TABLE IF EXISTS `matchCourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `matchCourse` (
  `courseID` int(11) NOT NULL,
  `TID` int(11) DEFAULT NULL,
  `SID` int(11) NOT NULL,
  PRIMARY KEY (`courseID`,`SID`),
  KEY `TID` (`TID`),
  KEY `SID` (`SID`),
  CONSTRAINT `matchcourse_ibfk_2` FOREIGN KEY (`TID`) REFERENCES `tutor` (`id`),
  CONSTRAINT `matchcourse_ibfk_3` FOREIGN KEY (`SID`) REFERENCES `student` (`id`),
  CONSTRAINT `matchcourse_ibfk_4` FOREIGN KEY (`courseID`) REFERENCES `course` (`courseid`),
  CONSTRAINT `matchcourse_ibfk_5` FOREIGN KEY (`SID`) REFERENCES `course` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matchCourse`
--

LOCK TABLES `matchCourse` WRITE;
/*!40000 ALTER TABLE `matchCourse` DISABLE KEYS */;
INSERT INTO `matchCourse` VALUES (1,100001,200001),(2,100003,200002),(6,100007,200005),(12,100007,200012),(3,100008,200003);
/*!40000 ALTER TABLE `matchCourse` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07  8:27:46
