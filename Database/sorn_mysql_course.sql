-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: sorn_mysql
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course` (
  `courseID` int(11) NOT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `detail` varchar(200) DEFAULT NULL,
  `SID` int(11) NOT NULL,
  `day` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL COMMENT 'per hours',
  `place` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`courseID`),
  KEY `SID` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'math','สอนวันเสาร์อาทิต ช่วงเช้าค่ะ ค่าเรียนชท. ละ 200 บาท อาทิตละสองชั่วโมง',200001,'sat','เช้า','300','bangrak'),(2,'physic','หาคนสอนฟิสิกด่วนๆ',200002,'sat','บ่าย','250','samyan'),(3,'biology','หาพี่ผู้หญิงาอนชีวะใจดีเป็นกันเองค่ะ',200003,'sat','เช้า','250','samyan'),(4,'chem','เคมี ม.2 ขึ้นม.3 เพิ่มเกรดและสอบเข้าม.4 ขอพี่ญ/ ร้านกาแฟพันธุ์ไทย เยื้องโรงเรียนเซนคาเบียล/',200007,'mon','เย็น','200','siam'),(5,'chem',': เคมี * ขอผญ.\n- ญ.1 ม.5 (อำมาตกุล)\n- พหลโยธิน 48 ปากซอย',200012,'mon','เย็น','200','silom'),(6,'biology',' วิชา: GED Science  ยังไม่มีกำหนดสอบ\nนักเรียน: ชาย 16 ปี\nติวเตอร์: ชาย\nวันเวลา: น้องว่างทุกวัน เช้าหรือบ่าย เน้นชีวะเป็นหลัก',200005,'sun','เช้า','200','siam'),(7,'english','วิชา: ภาษาอังกฤษ สนทนา เบื้องต้น\nนักเรียน: ชาย 10 ขวบ พื้นฐานน้อง รร. อัสสัมชัญ\nติวเตอร์: หญิง/ชาย สำเนียงดี',200001,'thu','บ่าย','200','asok'),(8,'english','SAT Eng \nนักเรียน: ชาย 18 ปี\nติวเตอร์: ชาย/หญิง',200001,'wed','บ่าย','200','asok'),(9,'thai','ภาษาไทย เกรด 3 และ เกรด 8 (ป.3 และ ม. 2) นักเรียน: ชาย ลูกครึ่งเกาหลี-ไทย พูดไทยได้ รร. นานาชาติ ติวเตอร์ : หญิง เอกภาษาไทย',200020,'tue','เช้า','250','siam'),(10,'eng','อังกฤษ ม. 5 และเตรียมพร้อมสอบ GAT อังกฤษ\nนักเรียน: หญิง รร. ซางตาครูซ\nติวเตอร์: หญิง มีประสบการณ์สอน',200003,'sat','เช้า','250','satorn'),(11,'math',' วิชา: คณิตศาสตร์ ป.6 (เพิ่มความรู้)\nนักเรียน: ชาย อายุ 12 ปี รร.วัดนิมมานรดี หลักสูตรไทย\nติวเตอร์:หญิง',200008,'sun','บ่าย','200','satorn'),(12,'math',' IGCSE Math\nนักเรียน: หญิง grade 9 รร.เซนโยฯ\nติวเตอร์: หญิง ใจเย็น มีประสบการณ์สอน',200017,'mon','บ่าย','300','samyan'),(13,'math',' วิชา: คณิต ม. 2\nนักเรียน: ชาย รร. เซนต์ดอมินิก\nติวเตอร์: หญิง/ชาย มีประสบการณ์สอน',200002,'sat','บ่าย','250','samyan'),(14,'physic',' วิชา: ฟิสิกส์ ม. 4  \nนักเรียน: หญิง รร. ราชินี\nติวเตอร์: หญิง',200005,'sun','เย็น','250','siam');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-07  8:27:46
