-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: localhost    Database: sorn_mysql
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `course` (
  `courseID` int(11) NOT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `detail` varchar(200) DEFAULT NULL,
  `SID` int(11) NOT NULL,
  `day` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  `price` varchar(45) DEFAULT NULL COMMENT 'per hours',
  `place` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`courseID`),
  KEY `SID` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (1,'math','สอนวันเสาร์อาทิต ช่วงเช้าค่ะ ค่าเรียนชท. ละ 200 บาท อาทิตละสองชั่วโมง',200001,'sat','เช้า',NULL,NULL),(2,'physic','หาคนสอนฟิสิกด่วนๆ',200002,'sat','บ่าย',NULL,NULL),(3,'biology','หาพี่ผู้หญิงาอนชีวะใจดีเป็นกันเองค่ะ',200003,'sat','เช้า',NULL,NULL),(4,'chem',NULL,200007,'mon','เย็น',NULL,NULL),(5,'chem',NULL,200012,'mon','เย็น',NULL,NULL),(6,'biology',NULL,200005,'sun','เช้า',NULL,NULL),(7,'english',NULL,200001,'thu','บ่าย',NULL,NULL),(8,'english',NULL,200001,'wed','บ่าย',NULL,NULL),(9,'thai',NULL,200020,'tue','เช้า',NULL,NULL),(10,'eng',NULL,200003,'sat','เช้า',NULL,NULL),(11,'math',NULL,200008,'sun','บ่าย',NULL,NULL),(12,'math',NULL,200017,'mon','บ่าย',NULL,NULL),(13,'math',NULL,200002,'sat','บ่าย',NULL,NULL),(14,'physic',NULL,200005,'sun','เย็น',NULL,NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matchCourse`
--

DROP TABLE IF EXISTS `matchCourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `matchCourse` (
  `courseID` int(11) NOT NULL,
  `TID` int(11) DEFAULT NULL,
  `SID` int(11) NOT NULL,
  PRIMARY KEY (`courseID`,`SID`),
  KEY `TID` (`TID`),
  KEY `SID` (`SID`),
  CONSTRAINT `matchcourse_ibfk_2` FOREIGN KEY (`TID`) REFERENCES `tutor` (`id`),
  CONSTRAINT `matchcourse_ibfk_3` FOREIGN KEY (`SID`) REFERENCES `student` (`id`),
  CONSTRAINT `matchcourse_ibfk_4` FOREIGN KEY (`courseID`) REFERENCES `course` (`courseid`),
  CONSTRAINT `matchcourse_ibfk_5` FOREIGN KEY (`SID`) REFERENCES `course` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matchCourse`
--

LOCK TABLES `matchCourse` WRITE;
/*!40000 ALTER TABLE `matchCourse` DISABLE KEYS */;
INSERT INTO `matchCourse` VALUES (1,100001,200001),(2,100003,200002),(6,100007,200005),(12,100007,200012),(3,100008,200003);
/*!40000 ALTER TABLE `matchCourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `ID` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `Contact` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (200001,'เด็กชาย ก','ไก่เคเอฟซี','@line12'),(200002,'เด็กชาย ข','ชอบกินไข่','@igAccount'),(200003,'ตายนู่','ไม่ได้เป็นนายก','facebook.com'),(200004,'ฮีม','เพื่อนลี','facebook12'),(200005,'กอลฟฟี่','ทูพีช','golfy2peace23'),(200006,'James','White','line @jamess25'),(200007,'Walter','White','Heisenberg1880'),(200008,'Jessy','Pinkman','Cap\'n Cook'),(200009,'Jammy','McGrill','Saulgoodman'),(200010,'Gus','Fring','Gustavo'),(200011,'Frank','Castle','ThePunisher'),(200012,'Tony','Stark','ImAnIronMan'),(200013,'Stan','Lee','MarvsOwner'),(200014,'Javis','Javis','CasualAI'),(200015,'Jon','Snow','KingInTheNorth'),(200016,'Sansa','Stark','LadyStark'),(200017,'Arya','Stark','FacelessGrill'),(200018,'Daenerys','Targaryen','MotherOfDragons'),(200019,'Bran','Stark','LittleLord'),(200020,'Joffrey','Baratheon','TheKingOnTheIronThrone');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor`
--

DROP TABLE IF EXISTS `tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tutor` (
  `ID` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `rating` int(11) DEFAULT NULL COMMENT 'rating 1-5',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor`
--

LOCK TABLES `tutor` WRITE;
/*!40000 ALTER TABLE `tutor` DISABLE KEYS */;
INSERT INTO `tutor` VALUES (100001,'Thayakorn','Fu',5),(100002,'Phannawhat','Pongkham',5),(100003,'Nhong','Ondemand',4),(100004,'Toey','Ondemand',4),(100005,'Vivian','Ondemand',5),(100006,'Tap','Alevel',3),(100007,'อุไรวรรณ','ศิวะกุล',5),(100008,'Thanapat','Lee',5);
/*!40000 ALTER TABLE `tutor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-06 12:17:43
