-- MySQL dump 10.13  Distrib 5.7.23, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: sorn_mysql
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `courseID` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(45) DEFAULT NULL,
  `detail` varchar(200) DEFAULT NULL,
  `SID` int(11) NOT NULL,
  `day` varchar(45) DEFAULT NULL,
  `time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`courseID`),
  KEY `SID` (`SID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` VALUES (2,'physic','หาคนสอนฟิสิกด่วนๆ',200002,'sat','บ่าย'),(3,'biology','หาพี่ผู้หญิงาอนชีวะใจดีเป็นกันเองค่ะ',200003,'sat','เช้า'),(4,'chem',NULL,200007,'mon','เย็น'),(5,'chem',NULL,200012,'mon','เย็น'),(6,'biology',NULL,200005,'sun','เช้า'),(7,'english',NULL,200001,'thu','บ่าย'),(8,'english',NULL,200001,'wed','บ่าย'),(9,'thai',NULL,200020,'tue','เช้า'),(10,'eng',NULL,200003,'sat','เช้า'),(11,'math',NULL,200008,'sun','บ่าย'),(14,'physic',NULL,200005,'sun','เย็น'),(18,'music',NULL,200001,'tue',NULL);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `Contact` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (200001,'เด็กชาย ก','ไก่เคเอฟซี','@line12'),(200002,'เด็กชาย ข','ชอบกินไข่','@igAccount'),(200003,'ตายนู่','ไม่ได้เป็นนายก','facebook.com'),(200004,'ฮีม','เพื่อนลี','facebook12'),(200005,'กอลฟฟี่','ทูพีช','golfy2peace23'),(200006,'James','White','line @jamess25'),(200007,'Walter','White','Heisenberg1880'),(200008,'Jessy','Pinkman','Cap\'n Cook'),(200009,'Jammy','McGrill','Saulgoodman'),(200010,'Gus','Fring','Gustavo'),(200011,'Frank','Castle','ThePunisher'),(200012,'Tony','Stark','ImAnIronMan'),(200013,'Stan','Lee','MarvsOwner'),(200014,'Javis','Javis','CasualAI'),(200015,'Jon','Snow','KingInTheNorth'),(200016,'Sansa','Stark','LadyStark'),(200017,'Arya','Stark','FacelessGrill'),(200018,'Daenerys','Targaryen','MotherOfDragons'),(200019,'Bran','Stark','LittleLord'),(200020,'Joffrey','Baratheon','TheKingOnTheIronThrone');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor`
--

DROP TABLE IF EXISTS `tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `rating` int(11) DEFAULT NULL COMMENT 'rating 1-5',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor`
--

LOCK TABLES `tutor` WRITE;
/*!40000 ALTER TABLE `tutor` DISABLE KEYS */;
INSERT INTO `tutor` VALUES (100001,'Thayakorn','Fu',5),(100002,'Phannawhat','Pongkham',5),(100003,'Nhong','Ondemand',4),(100004,'Toey','Ondemand',4),(100005,'Vivian','Ondemand',5),(100006,'Tap','Alevel',3),(100007,'อุไรวรรณ','ศิวะกุล',5),(100008,'Thanapat','Lee',5);
/*!40000 ALTER TABLE `tutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `passwd` varchar(20) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `contact` varchar(30) DEFAULT NULL,
  `Userstatus` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'guide619','guide619','thayakorn','fu','0000000000','Student'),(2,'guide1','guide1','Thayakorn','Fu','59310273','Student'),(3,'guide619','guide619','','','','Student'),(4,'awd','','','','','Student'),(5,'guide619','guide619','','','','Student'),(6,'','','','','','Student'),(7,'','','','','','Student'),(8,'','','','','','Student'),(9,'','','','','','Student'),(10,'','','','','','Student'),(11,'guide619','guide619','','','','Student'),(12,'','','','','','Student'),(13,'','','','','','Student'),(14,'','','','','','Student'),(15,'','','','','','Student'),(16,'guide619','guide619','','','','Student'),(17,'guide619','guide619','','','','Student'),(18,'guide2','guide2','thayakorn','fu','0000000000','Student'),(19,'guide2','guide2','thayakorn','fu','0000000000','Student'),(20,'guide2','guide2','thayakorn','fu','0000000000','Student'),(21,'guide2','guide2','thayakorn','fu','0000000000','Student'),(22,'guide2','guide2','thayakorn','fu','0000000000','Student'),(23,'guide2','guide2','thayakorn','fu','0000000000','Student'),(24,'guide619','guide619','kkk','lll','0999999999','Student'),(25,'guide619','guide619','sdasd','asdasd','00000000','tutor'),(26,'123456','123456','efsaf','dsfsd','00000000','Student'),(27,'guide4','guide4','hdfas','asdasd','000000000','tutor'),(28,'guide6199','guide619','asdas','asdasd','asd','Student'),(29,'guide6191','guide619','guide619','guidqs','asdasd','Student'),(30,'guide61911','guide619','asdas','asdasd','asdasd','Student'),(31,'guide61911asdas','guide619','asdas','asdasd','asdasd','Student'),(32,'guide61911asdasa','guide619','asdas','asdasd','asdasd','Student'),(33,'guide619111111','guide619','asd','asdasd','asdasd','Student'),(34,'guide618','guide619','111','11','111','student'),(35,'qqqq','qqq','qqq','qqq','qqq','tutor');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-26 16:34:04
